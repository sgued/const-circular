/// permforms the euclidian reminder and the conversion
/// between isize and usize
pub const fn eremc(left: isize, right: usize) -> usize {
    left.rem_euclid(right as isize) as usize
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn rem_euclid_convert3() {
        assert_eq!(eremc(-9, 3), 0);
        assert_eq!(eremc(-8, 3), 1);
        assert_eq!(eremc(-7, 3), 2);
        assert_eq!(eremc(-6, 3), 0);
        assert_eq!(eremc(-5, 3), 1);
        assert_eq!(eremc(-4, 3), 2);
        assert_eq!(eremc(-3, 3), 0);
        assert_eq!(eremc(-2, 3), 1);
        assert_eq!(eremc(-1, 3), 2);
        assert_eq!(eremc(0, 3), 0);
        assert_eq!(eremc(1, 3), 1);
        assert_eq!(eremc(2, 3), 2);
        assert_eq!(eremc(3, 3), 0);
        assert_eq!(eremc(4, 3), 1);
        assert_eq!(eremc(5, 3), 2);
        assert_eq!(eremc(6, 3), 0);
        assert_eq!(eremc(7, 3), 1);
        assert_eq!(eremc(8, 3), 2);
        assert_eq!(eremc(9, 3), 0);
    }

    #[test]
    fn rem_euclid_convert5() {
        assert_eq!(eremc(-9, 5), 1);
        assert_eq!(eremc(-8, 5), 2);
        assert_eq!(eremc(-7, 5), 3);
        assert_eq!(eremc(-6, 5), 4);
        assert_eq!(eremc(-5, 5), 0);
        assert_eq!(eremc(-4, 5), 1);
        assert_eq!(eremc(-3, 5), 2);
        assert_eq!(eremc(-2, 5), 3);
        assert_eq!(eremc(-1, 5), 4);
        assert_eq!(eremc(0, 5), 0);
        assert_eq!(eremc(1, 5), 1);
        assert_eq!(eremc(2, 5), 2);
        assert_eq!(eremc(3, 5), 3);
        assert_eq!(eremc(4, 5), 4);
        assert_eq!(eremc(5, 5), 0);
        assert_eq!(eremc(6, 5), 1);
        assert_eq!(eremc(7, 5), 2);
        assert_eq!(eremc(8, 5), 3);
        assert_eq!(eremc(9, 5), 4);
    }
}
