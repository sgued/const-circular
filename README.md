const-circular
==============

This is a library made to experiment and learn to use unsafe Rust and const generics.
It implements a basic circular buffer of size known at compile time. This means that the buffer itself can be inlined with the struct and that it can therefore be used without an allocator.

It is meant just as a learning exercise and you probably shouldn't use it.

The nightly compiler is required as this makes use of unstable APIs of `MaybeUninit`
